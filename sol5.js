// sum of salary based on country (obj)

const data = require('./p1.js');
const func = require('./sol3.js');
const correct_data = func(data);

let salaryCountry = arr =>{
    
    // NOTE: all the commented code is for my reference..


    //use reduce instead

    let salaryByCountry = arr.reduce((accumulator,current)=>{
        let key = current.location;
            if(accumulator.hasOwnProperty(key)){
            accumulator[key] += current.corrected_salary;
        }else{
             accumulator[key] = current.corrected_salary;
        }
        return accumulator
    },{} /*this is the empty object equivalent to "newObj = {}" */)

    //console.log(salaryByCountry);
    console.log(salaryByCountry);
    return salaryByCountry;
    /*let newObj = {};
    arr.forEach(element=>{
        
        let key = element['location'];
        if(newObj.hasOwnProperty(key)){
             newObj[key]+= element.corrected_salary;
        }else{
             newObj[key] = element.corrected_salary;
        }
    })*/
    //console.log(newObj)
    //return newObj;
    /*for(let i in arr){
        if(!newObj.hasOwnProperty(arr[i]['location'])){
            newObj[arr[i].location] = 0;
        }
    }
    //console.log(newObj);
    for(let i in newObj){
        let salary = 0;
        for(let j in arr){
            if(i == arr[j].location){
                salary += arr[j].corrected_salary ;
            }
        }
        newObj[i] = salary;
    }
   console.log(newObj)
}*/
}
salaryCountry(correct_data);
module.exports = salaryCountry ;