// avg. salary per country
const data = require('./p1.js');
const func = require('./sol3.js');
const correct_data = func(data);

// console.log(salaryByCountry(correct_data));

let avgCountrySalary = arr =>{

    //use reduce :-

    let keysArr = arr.reduce((accumulator,current)=>{

        if(!accumulator.hasOwnProperty(current.location)){
            accumulator[current.location] = {count: 1, salary: current.corrected_salary} ;
        }else{
            accumulator[current.location].count ++ ;
            accumulator[current.location].salary += current.corrected_salary ;
        }

        return accumulator ;
    },{});
    console.log((keysArr));

    const countries = Object.entries(keysArr);
    console.log(countries);

    let finalArr = countries.reduce((accumulator, country)=>{
        let location = country[0];
        let avg = parseFloat((country[1].salary/country[1].count).toFixed(3));
        if(!accumulator.hasOwnProperty(location)){
            accumulator[location] = avg;
        }

        return accumulator;

    },{})

    console.log(finalArr);


    

    /*newObj = {};
    for(let i in arr){
        if(!newObj.hasOwnProperty(arr[i]['location'])){
            newObj[arr[i].location] = 0;
        }
    }
    for(let i in newObj){
        let count = 0;
        let salary = 0;
        for(let j in arr){
            if(i == arr[j].location){
                count ++ ;
                salary += arr[j].corrected_salary ;
            }
        }
        newObj[i] = salary/count;
    }
    console.log(newObj)*/
}

avgCountrySalary(correct_data);